package HBaseToCSV;

import com.opencsv.CSVWriter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.util.List;

public class CSVCreationUtil {

    public void writeToCSV(List<PeopleInfo> peopleInfoList){

        String uri = "hdfs://localhost:9000";
        String hdfsDir = "hdfs://localhost:9000/Assignment2B/People_Info.csv";

        try{
            Configuration config = HBaseConfiguration.create();
            FileSystem hdfs = FileSystem.get(new URI(uri), config);

            Path filePath = new Path(hdfsDir);
            if(hdfs.exists(filePath)){
                hdfs.delete(filePath, true);
            }

            OutputStream os = hdfs.create(filePath, () -> System.out.println("...bytes written: []"));

            CSVWriter csvWriter = new CSVWriter(new OutputStreamWriter(os, "UTF-8"));
            for(PeopleInfo peopleInfo : peopleInfoList){
                csvWriter.writeNext(peopleInfo.toArray());
            }

            csvWriter.close();
            hdfs.close();
            os.close();

        } catch(Exception e){
            e.printStackTrace();
        }
    }
}

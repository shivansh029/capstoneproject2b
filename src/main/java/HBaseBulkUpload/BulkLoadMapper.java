package HBaseBulkUpload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class BulkLoadMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    private String hBaseTable;
    private String dataSeparator;
    private String columnFamily;
    private ImmutableBytesWritable hBaseTableName;

    public void setup(Context context){
        Configuration config = context.getConfiguration();
        hBaseTable = config.get("hbase.table.name");
        dataSeparator = config.get("data.separator");
        columnFamily = config.get("COLUMN_FAMILY");
        hBaseTableName = new ImmutableBytesWritable(Bytes.toBytes(hBaseTable));
    }

    public void map(LongWritable key, Text value, Context context){
        try{

            String[] values = value.toString().split(dataSeparator);
            System.out.println("Row : " + values[1]);
            String rowkey = values[0].replaceAll("\"", "");
            Put put = new Put(Bytes.toBytes(rowkey));

            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("name"), Bytes.toBytes(values[1].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("age"), Bytes.toBytes(values[2].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("company"), Bytes.toBytes(values[3].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("building_code"), Bytes.toBytes(values[4].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("phone_number"), Bytes.toBytes(values[5].replaceAll("\"", "")));
            put.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("address"), Bytes.toBytes(values[6].replaceAll("\"", "")));
            context.write(hBaseTableName, put);

        } catch(Exception e){
            e.printStackTrace();
        }
    }
}

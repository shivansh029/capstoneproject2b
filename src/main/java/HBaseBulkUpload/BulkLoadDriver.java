package HBaseBulkUpload;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.net.URI;

public class BulkLoadDriver extends Configured implements Tool {

    private static final String DATA_SEPARATOR = ",";
    private static final String TABLE_NAME = "People_Info_Bulk_Upload";
    private static final String COLUMN_FAMILY = "Details";

    public static void main(String[] args) {
        try{

            int response = ToolRunner.run(HBaseConfiguration.create(), new BulkLoadDriver(), args);
            if(response == 0){
                System.out.println("Job is successfully completed.");
            }
            else{
                System.out.println("Job failed.");
            }

        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        int result = 0;
        String inputPath = "hdfs://localhost:9000/Assignment2B/People_Info.csv";
        String outputPath = "hdfs://localhost:9000/Assignment2B/hFiles/out/";

        Configuration config = HBaseConfiguration.create();
        config.set("data.separator", DATA_SEPARATOR);
        config.set("hbase.table.name", TABLE_NAME);
        config.set("COLUMN_FAMILY", COLUMN_FAMILY);
        config.set("hbase.fs.tmp.dir", "/Users/shivansh/Projects/hbase-staging");

        Job job = Job.getInstance(config);
        job.setJarByClass(BulkLoadDriver.class);
        job.setJobName("Bulk Loading HBASE Table : " + TABLE_NAME);
        job.setInputFormatClass(TextInputFormat.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapperClass(BulkLoadMapper.class);

        FileInputFormat.addInputPaths(job, inputPath);
        FileSystem.get(new URI(outputPath), config).delete(new Path(outputPath), true);
        FileOutputFormat.setOutputPath(job, new Path(outputPath));
        job.setMapOutputValueClass(Put.class);
        Connection connection = ConnectionFactory.createConnection(config);
        Table hTable = connection.getTable(TableName.valueOf(TABLE_NAME));
        RegionLocator region = connection.getRegionLocator(TableName.valueOf(TABLE_NAME));
        HFileOutputFormat2.configureIncrementalLoad(job, hTable, region);
        job.waitForCompletion(true);

        if(job.isSuccessful()){
            HBaseBulkLoad.doBulkLoad(outputPath, TABLE_NAME);
        }
        else{
            result = -1;
        }

        return result;
    }
}
